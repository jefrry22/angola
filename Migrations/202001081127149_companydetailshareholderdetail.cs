﻿namespace Angola.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class companydetailshareholderdetail : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ShareHolderDetails",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        ShareHolderdetails = c.String(),
                        ShareHolderdetails1 = c.String(),
                        ShareHolderdetails2 = c.String(),
                        details_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CompanyDetails", t => t.details_id)
                .Index(t => t.details_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ShareHolderDetails", "details_id", "dbo.CompanyDetails");
            DropIndex("dbo.ShareHolderDetails", new[] { "details_id" });
            DropTable("dbo.ShareHolderDetails");
        }
    }
}
