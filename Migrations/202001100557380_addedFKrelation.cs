﻿namespace Angola.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedFKrelation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ShareHolderDetails", "details_id", c => c.Int());
            CreateIndex("dbo.ShareHolderDetails", "details_id");
            AddForeignKey("dbo.ShareHolderDetails", "details_id", "dbo.CompanyDetails", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ShareHolderDetails", "details_id", "dbo.CompanyDetails");
            DropIndex("dbo.ShareHolderDetails", new[] { "details_id" });
            DropColumn("dbo.ShareHolderDetails", "details_id");
        }
    }
}
