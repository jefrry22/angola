﻿namespace Angola.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updshareholderdetail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ShareHolderDetails", "Shareholder1", c => c.String());
            AddColumn("dbo.ShareHolderDetails", "Address1", c => c.String());
            AddColumn("dbo.ShareHolderDetails", "Birthplace1", c => c.String());
            AddColumn("dbo.ShareHolderDetails", "ID1", c => c.String());
            AddColumn("dbo.ShareHolderDetails", "Shareholder2", c => c.String());
            AddColumn("dbo.ShareHolderDetails", "Address2", c => c.String());
            AddColumn("dbo.ShareHolderDetails", "Birthplace2", c => c.String());
            AddColumn("dbo.ShareHolderDetails", "ID2", c => c.String());
            AddColumn("dbo.ShareHolderDetails", "Shareholder3", c => c.String());
            AddColumn("dbo.ShareHolderDetails", "Address3", c => c.String());
            AddColumn("dbo.ShareHolderDetails", "Birthplace3", c => c.String());
            AddColumn("dbo.ShareHolderDetails", "ID3", c => c.String());
            DropColumn("dbo.ShareHolderDetails", "ShareHolderdetails");
            DropColumn("dbo.ShareHolderDetails", "ShareHolderdetails1");
            DropColumn("dbo.ShareHolderDetails", "ShareHolderdetails2");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ShareHolderDetails", "ShareHolderdetails2", c => c.String());
            AddColumn("dbo.ShareHolderDetails", "ShareHolderdetails1", c => c.String());
            AddColumn("dbo.ShareHolderDetails", "ShareHolderdetails", c => c.String());
            DropColumn("dbo.ShareHolderDetails", "ID3");
            DropColumn("dbo.ShareHolderDetails", "Birthplace3");
            DropColumn("dbo.ShareHolderDetails", "Address3");
            DropColumn("dbo.ShareHolderDetails", "Shareholder3");
            DropColumn("dbo.ShareHolderDetails", "ID2");
            DropColumn("dbo.ShareHolderDetails", "Birthplace2");
            DropColumn("dbo.ShareHolderDetails", "Address2");
            DropColumn("dbo.ShareHolderDetails", "Shareholder2");
            DropColumn("dbo.ShareHolderDetails", "ID1");
            DropColumn("dbo.ShareHolderDetails", "Birthplace1");
            DropColumn("dbo.ShareHolderDetails", "Address1");
            DropColumn("dbo.ShareHolderDetails", "Shareholder1");
        }
    }
}
