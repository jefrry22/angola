﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Angola
{
    class ScrapeDbContext: DbContext
    {
        public ScrapeDbContext() : base("Name=ScrapeDbcontext")
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }
        public DbSet<CompanyDetails> companyDetails { get; set; }
        public DbSet<ShareHolderDetails> details { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

    }
}
