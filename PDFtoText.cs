﻿using org.apache.pdfbox.pdmodel;
using org.apache.pdfbox.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Angola
{
    class PDFtoText
    {
        public string ExtractText(string PDFFilePath)
        {
            ScrapeDbContext context = new ScrapeDbContext();
            CompanyDetails company = new CompanyDetails();
            ShareHolderDetails holder = new ShareHolderDetails();
            IDictionary<string, string> d = new Dictionary<string, string>();
            IDictionary<string, string> date = new Dictionary<string, string>();
            IDictionary<string, string> share = new Dictionary<string, string>();
            IDictionary<string, string> place = new Dictionary<string, string>();
            IDictionary<string, string> share1 = new Dictionary<string, string>();
            PDDocument doc = PDDocument.load(PDFFilePath);
            PDFTextStripper stripper = new PDFTextStripper();
            stripper.setStartPage(1);
            stripper.setEndPage(stripper.getEndPage());
            string txt = stripper.getText(doc);

            int start = txt.LastIndexOf("SUMÁRIO");
            int end = txt.LastIndexOf("Limitada».");
            string Companyname = txt.Substring(start, end - start).Replace("Umitada", "Limitada").Trim();
            string[] readlines = Companyname.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);



            PDFTextStripper docforIndex = new PDFTextStripper();
            string details = docforIndex.getText(doc).Trim();
            string[] documents = details.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            List<string> Companylist = new List<string>();
            string EndIndex = "";
            int startIndex = 0;
            for (int i = 0; i < documents.Length; i++)
            {
                if (documents[i].Contains("SUMÁRIO"))
                {
                    startIndex = i;
                    break;
                }
            }
            for (int i = startIndex; i < documents.Length; i++)
            {
                if (documents[i].ToLower().Contains("limitada") || documents[i].ToLower().Contains("umitada"))
                {
                    if ((Companylist.Count != 0) && (documents[i].ToLower().Trim() != EndIndex))
                        Companylist.Add(documents[i]);
                    else
                    {
                        if (documents[i].ToLower().Trim() == EndIndex)
                            break;
                        if (Companylist.Count == 0)
                            EndIndex = documents[i].ToLower().Trim();
                        Companylist.Add(documents[i]);
                    }
                }
                else if (i < documents.Length - 1)
                {
                    if (documents[i + 1].ToLower().Contains("limitada") || documents[i + 1].ToLower().Contains("umitada"))
                    {
                        if ((Companylist.Count != 0) && ((documents[i].ToLower().TrimStart() + documents[i + 1].ToLower().TrimEnd()) != EndIndex))
                        {
                            Companylist.Add(documents[i].ToLower().TrimStart() + documents[i + 1].ToLower().TrimEnd());
                            i++;
                        }
                        else
                        {
                            if ((documents[i].ToLower().TrimStart() + documents[i + 1].ToLower().TrimEnd()) == EndIndex)
                                break;
                            if (Companylist.Count == 0)
                                EndIndex = documents[i].ToLower().TrimStart() + documents[i + 1].ToLower().TrimEnd();
                            Companylist.Add(documents[i].ToLower().TrimStart() + documents[i + 1].ToLower().TrimEnd());
                            i++;
                        }
                    }
                }
            }          
            PDFTextStripper pDF = new PDFTextStripper();
            pDF.setStartPage(3);
            int endpg = pDF.getEndPage();
            pDF.setEndPage(endpg);
            string detail = pDF.getText(doc).Trim();
            string[] document = detail.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
           

            foreach (var line in readlines)
            {
                
                if (line.Contains("Limitada") || line.Contains("limitada"))
                {
                    string CompanyInfoSection = "";
                    string doi = "";
                    string shareholder = "";
                    string shareholder1 = "";
                    string shareholder2 = "";
                    string shareholder3 = "";
                    string birthplace = "";
                    string birthplace1 = "";
                    string birthplace2 = "";
                    string birthplace3 = "";
                    string nationality = "";
                    string address = "";
                    string address1 = "";
                    string address2 = "";
                    string address3 = "";
                    string em = "";
                    string id = "";
                    string id1 = "";
                    string id2 = "";
                    string headquarters = "";
                    string management = "";
                    string companynum = "";
                    string activities = "";
                    string capital = "";
                    string owner = "";
                    string ownerholds = "";
                    int endl = 1;
                    int xy = 1;
                    string shareinfo = "";
                    string shareinfo1 = "";
                    string shareinfo2 = "";
                    string shareinfo3 = "";
                    string administration = "";
                    string tempsede = "";

                    bool ScrapingDone = false;
                    string indexline = line.Remove(line.Length - 2, 2).Trim().Replace(" ", "");
                    //string indexline = line.Remove(line.Length - 1, 1).Trim().Replace(" ", "");
                    for (int i = 0; i < document.Length; i++)
                    {

                        string documentline = document[i].Trim().Replace(" ", "");
                        if (documentline.ToLower().Contains(indexline.ToLower()))
                        {
                            string temp = documentline;
                            for (int j = i; j < document.Length; j++)
                            {
                                string doccontent = document[j].Trim().Replace(" ", "").ToLower();
                                if (!(doccontent.Contains("oajudante") || doccontent.Contains("aajudante") || doccontent.Contains("onotario") || doccontent.Contains("anotaria")))
                                    CompanyInfoSection = CompanyInfoSection + document[j];
                                else
                                {
                                    CompanyInfoSection = CompanyInfoSection + document[j];
                                    int n = CompanyInfoSection.Length;
                                    if (n < 400)
                                    {
                                        ScrapingDone = false;
                                        continue;
                                    }
                                    else
                                        ScrapingDone = true;
                                    break;
                                }
                                if (doccontent.Contains("constituída") || doccontent.Contains("constituida") || doccontent.Contains("constituição") || doccontent.Contains("contituem") || doccontent.Contains("constitue") || doccontent.Contains("contituição"))
                                {
                                    string tempd = doi + document[j - 1] + document[j] + document[j + 1];
                                    //int st = tempd.IndexOf(',');
                                    int en = tempd.LastIndexOf(',');
                                    if (!(en == -1))
                                    {
                                        doi = tempd.Substring(0, en);   // will give the line of date
                                        Match match = Regex.Match(doi, "[0-9]+[^0-9]+[0-9][0-9][0-9][0-9]");
                                        if (match.Success)
                                        {
                                            doi = match.Captures[0].Value; // Will output only date
                                            
                                        }
                                        //company.DateOfIncorporation = doi;
                                    }
                                }


                                //Step 15:
                                if (doccontent.Contains("casado") || doccontent.Contains("casada") || doccontent.Contains("solteiro") || doccontent.Contains("solteira"))
                                {
                                    string temps = shareholder + document[j - 1] + document[j] + document[j + 1] + document[j + 2] + document[j + 3] + document[j + 4] + document[j + 5];
                                    if(temps.Contains("nacionalidade"))
                                    {
                                        em = temps.Substring(temps.IndexOf("nacionalidade")+13);
                                        int e = em.IndexOf(',');
                                        nationality = em.Substring(0, e);                                        
                                    }
                                    if (temps.Contains("Primeiro:"))
                                    {
                                        int st = temps.IndexOf("Primeiro:") + 9;
                                        int en = temps.LastIndexOf(',');
                                        shareholder1 = temps.Substring(st, en - st);                                        
                                        Match match = Regex.Match(shareholder1, "[A-Z].*");
                                        if (match.Success)
                                        {
                                            shareholder1 = match.Value;
                                            if (shareholder1.Contains(','))
                                            {
                                                shareholder1 = shareholder1.Substring(0, shareholder1.IndexOf(','));
                                                break;
                                            }

                                        }
                                        if (temps.Contains("natural"))
                                        {
                                            int s = temps.IndexOf("natural");
                                            int e = temps.LastIndexOf(',');
                                            birthplace1 = temps.Substring(s, e - s);
                                            match = Regex.Match(birthplace1, "\\b([A-Z][a-z]*)");
                                            if (match.Success)
                                            {
                                                birthplace1 = match.Value;

                                            }
                                        }
                                        if (doccontent.Contains("residente") || doccontent.Contains("reside"))
                                        {
                                            string tempa = address1 + document[j - 1] + document[j] + document[j + 1];
                                            int s = tempa.IndexOf("residente");
                                            int e = tempa.LastIndexOf(',');
                                            if (!(e == -1))
                                            {
                                                address1 = tempa.Substring(s, e - s);
                                                match = Regex.Match(address1, "([A-Z][a-z']*)");
                                                if (match.Success)
                                                {
                                                    address1 = match.Value;
                                                }
                                            }
                                        }
                                        else if (temps.Contains("Primeira:"))
                                        {
                                            int s = temps.IndexOf("Primeira:") + 9;
                                            int e = temps.LastIndexOf(',');
                                            shareholder1 = temps.Substring(s, e - s);
                                            match = Regex.Match(shareholder1, "[A-Z].*");
                                            if (match.Success)
                                            {
                                                shareholder1 = match.Value;
                                                if (shareholder1.Contains(','))
                                                {
                                                    shareholder1 = shareholder1.Substring(0, shareholder1.IndexOf(','));
                                                }

                                            }
                                            if (temps.Contains("natural"))
                                            {
                                                int s1 = temps.IndexOf("natural");
                                                int s2 = temps.LastIndexOf(',');
                                                birthplace1 = temps.Substring(s, e - s);
                                                match = Regex.Match(birthplace1, "\\b([A-Z][a-z]*)");
                                                if (match.Success)
                                                {
                                                    birthplace1 = match.Value;

                                                }
                                            }
                                            if (doccontent.Contains("residente") || doccontent.Contains("reside"))
                                            {
                                                string tempa = address1 + document[j - 1] + document[j] + document[j + 1];
                                                int startin = tempa.IndexOf("residente");
                                                int endin = tempa.LastIndexOf(',');
                                                if (!(endin == -1))
                                                {
                                                    address1 = tempa.Substring(startin, endin - startin);
                                                    match = Regex.Match(address1, "([A-Z][a-z']*)");
                                                    if (match.Success)
                                                    {
                                                        address1 = match.Value;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (temps.Contains("Segundo:"))
                                    {
                                        int st = temps.IndexOf("Segundo:") + 8;
                                        int en = temps.LastIndexOf(',');
                                        shareholder2 = temps.Substring(st, en - st);
                                        Match match = Regex.Match(shareholder2, "[A-Z].*");
                                        if (match.Success)
                                        {
                                            shareholder2 = match.Value;
                                            if (shareholder2.Contains(','))
                                            {
                                                shareholder2 = shareholder2.Substring(0, shareholder2.IndexOf(','));
                                            }
                                        }
                                        if (doccontent.Contains("residente") || doccontent.Contains("reside"))
                                        {
                                            string tempa = address2 + document[j - 1] + document[j] + document[j + 1];
                                            int startin = tempa.IndexOf("residente");
                                            int endin = tempa.LastIndexOf(',');
                                            if (!(endin == -1))
                                            {
                                                address2 = tempa.Substring(startin, endin - startin);
                                                match = Regex.Match(address2, "([A-Z][a-z']*)");
                                                if (match.Success)
                                                {
                                                    address2 = match.Value;
                                                }
                                            }
                                        }
                                        if (temps.Contains("natural"))
                                        {
                                            int s1 = temps.IndexOf("natural");
                                            int s2 = temps.LastIndexOf(',');
                                            birthplace2 = temps.Substring(s1, s2 - s1);
                                            match = Regex.Match(birthplace2, "\\b([A-Z][a-z]*)");
                                            if (match.Success)
                                            {
                                                birthplace1 = match.Value;

                                            }
                                        }
                                    }
                                    if (temps.Contains("Terceiro:"))
                                    {
                                        int st = temps.IndexOf("Terceiro:") + 9;
                                        int en = temps.LastIndexOf(',');
                                        shareholder3 = temps.Substring(st, en - st);
                                        Match match = Regex.Match(shareholder3, "[A-Z].*");
                                        if (match.Success)
                                        {
                                            shareholder3 = match.Value;
                                            if (shareholder3.Contains(','))
                                            {
                                                shareholder3 = shareholder3.Substring(0, shareholder3.IndexOf(','));
                                            }
                                        }
                                        if (doccontent.Contains("residente") || doccontent.Contains("reside"))
                                        {
                                            string tempa = address3 + document[j - 1] + document[j] + document[j + 1];
                                            int startin = tempa.IndexOf("residente");
                                            int endin = tempa.LastIndexOf(',');
                                            if (!(endin == -1))
                                            {
                                                address3 = tempa.Substring(startin, endin - startin);
                                                match = Regex.Match(address3, "([A-Z][a-z']*)");
                                                if (match.Success)
                                                {
                                                    address3 = match.Value;
                                                }
                                            }
                                        }
                                        if (temps.Contains("natural"))
                                        {
                                            int s1 = temps.IndexOf("natural");
                                            int s2 = temps.LastIndexOf(',');
                                            birthplace3 = temps.Substring(s1, s2 - s1);
                                            match = Regex.Match(birthplace3, "\\b([A-Z][a-z]*)");
                                            if (match.Success)
                                            {
                                                birthplace1 = match.Value;

                                            }
                                        }
                                    }
                                    else
                                    {
                                        int e = temps.IndexOf(',');
                                        if (!(e == -1))
                                        {
                                            shareholder = temps.Substring(0, e);
                                            //Match match = Regex.Match(shareholder, "\\b([A-Z][a-z]*)(\\s[A-Z]*[a-z]*)\\b");
                                            Match match = Regex.Match(shareholder, "[A-Z].*");
                                            if (match.Success)
                                            {
                                                shareholder = match.Value;
                                                if (shareholder.Contains(','))
                                                {
                                                    shareholder = shareholder.Substring(0, shareholder.IndexOf(','));
                                                }
                                            }
                                        }
                                        if (temps.Contains("nacionalidade"))
                                        {
                                            string tempcap = em + document[j] + document[j + 1] + document[j + 2];
                                            int offset = tempcap.IndexOf(",");
                                            int st = tempcap.IndexOf("nacionalidade") + 13;
                                            for (xy = 1; xy < 10; xy++)
                                            {

                                                offset = tempcap.IndexOf(",", offset + 1);
                                                if (tempcap.IndexOf("nacion") == 0)
                                                {
                                                    int sta = tempcap.IndexOf("nacionalidade") + 13;
                                                    int en = tempcap.IndexOf(",");
                                                    nationality = tempcap.Substring(sta, en - st);
                                                    break;

                                                }
                                                if (offset > st)
                                                {
                                                    int sta = tempcap.IndexOf("nacionalidade") + 13;
                                                    int en = offset;
                                                    nationality = tempcap.Substring(sta, en - st);
                                                    break;
                                                }

                                            }


                                        }
                                        if (temps.Contains("natural"))
                                        {
                                            int s1 = temps.IndexOf("natural");
                                            int s2 = temps.LastIndexOf(',');
                                            birthplace = temps.Substring(s1, s2 - s1);
                                            Match match = Regex.Match(birthplace, "\\b([A-Z][a-z]*)");
                                            if (match.Success)
                                            {
                                                birthplace = match.Value;

                                            }
                                        }
                                    }

                                }

                                //Step 15:
                                if (doccontent.Contains("residente"))
                                {
                                    string tempa = address + document[j - 1] + document[j] + document[j + 1] + document[j + 2];
                                    int s = tempa.IndexOf("residente");
                                    int e = tempa.LastIndexOf(',');
                                    if (!(e == -1))
                                    {
                                        address = tempa.Substring(s, e - s);
                                        Match match = Regex.Match(address, "([A-Z][a-z']*)");
                                        if (match.Success)
                                        {
                                            address = match.Value;
                                        }
                                        else
                                            e = tempa.LastIndexOf('.');
                                        address = tempa.Substring(s, e - s);
                                        Match match1 = Regex.Match(address, "([A-Z][a-z']*)");
                                        if (match1.Success)
                                        {
                                            address = match1.Value;
                                        }
                                    }

                                }
                                /*if (doccontent.Contains("titular"))
                                {
                                    string tempid = id + document[j] + document[j+1] ;
                                    int st = tempid.IndexOf("titular") + 7;
                                    int en = tempid.LastIndexOf('.');
                                    if (!(en == -1))
                                    {
                                        if (en > st)
                                        {
                                            id = tempid.Substring(st, en - st);
                                        }
                                        else
                                            en = tempid.LastIndexOf(',');
                                        id = tempid.Substring(st, en - st);
                                    }
                                }    */
                                //Step-18:
                                if (doccontent.Contains("contribuintefiscal") || doccontent.Contains("ContribuinteFiscal") || doccontent.Contains("nif.") || doccontent.Contains("NIF") || doccontent.Contains("n.i.f.") || doccontent.Contains("nif:") || doccontent.Contains("n.i.f.:") || doccontent.Contains("numerodeidentificacaofiscal") || doccontent.Contains("númerodeidentificaçãofiscal") || doccontent.Contains("matriculada"))
                                {
                                    string tempc = companynum + document[j] + document[j + 1]+document[j+2];
                                    int s = tempc.IndexOf('.');
                                    int e = tempc.LastIndexOf(',');
                                    if (e < s)
                                    {
                                        e = tempc.LastIndexOf('.');
                                    }

                                    if (!(e == -1))
                                    {
                                        companynum = tempc.Substring(s, e - s);   // will give the line of num
                                        int index = companynum.IndexOfAny(new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' });



                                        companynum = companynum.Substring(index, 10);
                                    }                                
                                }



                                //if (doccontent.Contains("nacionalidade"))
                                //{
                                //    string tempcap = em + document[j] + document[j + 1];
                                //    int offset = tempcap.IndexOf(",");
                                //    int st = tempcap.IndexOf("nacionalidade") + 13;
                                //    for (xy = 1; xy < 10; xy++)
                                //    {

                                //        offset = tempcap.IndexOf(",", offset + 1);
                                //        if (tempcap.IndexOf("nacion") == 0)
                                //        {
                                //            int sta = tempcap.IndexOf("nacionalidade") + 13;
                                //            int en = tempcap.IndexOf(",");
                                //            nationality = tempcap.Substring(sta, en - st);
                                //            break;

                                //        }
                                //        if (offset > st)
                                //        {
                                //            int sta = tempcap.IndexOf("nacionalidade") + 13;
                                //            int en = offset;
                                //            nationality = tempcap.Substring(sta, en - st);
                                //            break;
                                //        }

                                //    }


                                //}                                
                            }                           
                            if (CompanyInfoSection.Contains("sede"))
                            {

                                int st = CompanyInfoSection.IndexOf("ARTIGO") + 5;
                                int offset = CompanyInfoSection.LastIndexOf("ARTIGO") + 20;
                                if (st == 4)
                                {
                                    st = CompanyInfoSection.IndexOf("sede");
                                    offset = CompanyInfoSection.IndexOf("Objecto:");
                                    if(!(offset==-1)&&offset>st)
                                    headquarters = CompanyInfoSection.Substring(st, offset - st);
                                }
                                else
                                {
                                    string sub = CompanyInfoSection.Substring(st, offset - st);
                                    /*
                                                                    int sta = sub.IndexOf("(Sede");
                                                                    int off = sub.IndexOf("sede");

                                                                    string preString = "(Sede";
                                                                    string searchString = "sede";
                                                                    int preIndex = sub.IndexOf(preString);
                                                                    int searchIndex = preIndex + sub.Substring(preIndex).IndexOf(searchString);
                                                                    string cap = sub.Substring(preIndex, searchIndex - preIndex);
                                                                    int dot = sub.IndexOf("\n");*/
                                    string preStrin = "sede";
                                    string searchStrin = ".";
                                    int preInde = sub.IndexOf(preStrin) + 4;
                                    int searchInde = preInde + sub.Substring(preInde).IndexOf(searchStrin) + 1;

                                    headquarters = sub.Substring(preInde, searchInde - preInde);
                                }

                            }
                            //Step -25:
                            /*if(CompanyInfoSection.Contains("Gerência"))
                                {
                                int st = CompanyInfoSection.IndexOf("ARTIGO") + 5;
                                if(st==4)
                                {
                                    int sta1 = CompanyInfoSection.IndexOf("Gerência");                                    
                                    string sub1 = CompanyInfoSection.Substring(sta1);
                                    if (sub1.Contains("sócio")&&sub1.Contains("gerente"))
                                    {
                                        int off1 = sub1.IndexOf("sócio") + 5;
                                        if (!(off1==-1))
                                        { 
                                            sub1 = sub1.Substring(off1);
                                            off1 = sub1.IndexOf("gerente");
                                            sub1 = sub1.Substring(off1);
                                            int off2 = sub1.IndexOf('.');
                                            off1 = sub1.IndexOf("gerente") + 7;
                                            if (off2 == -1)
                                            {
                                                management = sub1.Substring(off1, sub1.IndexOf(',') - off1);
                                            }
                                            management = sub1.Substring(off1, off2 - off1);
                                        }                                        
                                        break;
                                    }                                   
                                }
                                int offset = CompanyInfoSection.LastIndexOf("ARTIGO") + 20;
                                if(!(offset==19))
                                { 
                                string sub = CompanyInfoSection.Substring(st, offset - st);

                                int sta = sub.IndexOf("gerência");
                                int off = sub.IndexOf("sócio");

                                string preString = "gerência";
                                string searchString = "sócio";
                                int c = 1;
                                while (c < 10)
                                {
                                    sta = sub.IndexOf("gerência", sta + 1);
                                    int preIndex = sub.IndexOf(preString);
                                    int searchIndex = preIndex + sub.Substring(preIndex).IndexOf(searchString);
                                        if (searchIndex > sta)
                                        {
                                            string cap = sub.Substring(preIndex, (searchIndex - preIndex) + 120);
                                            int dot = sub.IndexOf("\n");
                                            string preStrin = "sócio";
                                            string searchStrin = ",";
                                            int ind = cap.IndexOf(searchStrin);
                                            if (ind == -1)
                                            {
                                                searchStrin = ".";
                                            }
                                            int preInde = cap.IndexOf(preStrin) + 5;
                                            int searchInde = preInde + cap.Substring(preInde).IndexOf(searchStrin) + 1;

                                            management = cap.Substring(preInde, searchInde - preInde);
                                            if (management.Contains("MINJDH - Ministério da justiça e Direitos Humanos "))
                                            {
                                                var newhq = management.Replace("MINJDH - Ministério da justiça e Direitos Humanos ", "").Trim();
                                                var output = Regex.Replace(newhq, @"[\d-]", string.Empty);
                                                management = Regex.Replace(output, @"^\s*$\n|\r", string.Empty, RegexOptions.Multiline).TrimEnd();

                                            }
                                            c++;
                                        }
                                    }
                                }                               

                            } */

                            if (CompanyInfoSection.Contains("Objecto"))
                            {
                                int st = CompanyInfoSection.IndexOf("ARTIGO") + 5;
                                int offset = CompanyInfoSection.LastIndexOf("ARTIGO") + 20;
                                if (st == 4)
                                {
                                    st = CompanyInfoSection.IndexOf("Objecto");
                                    offset = CompanyInfoSection.IndexOf("Sócio");
                                    if (!(offset == -1))
                                        activities = CompanyInfoSection.Substring(st, offset - st);
                                    else
                                        offset = CompanyInfoSection.IndexOf("Sócia");
                                    if (!(offset == -1))
                                        activities = CompanyInfoSection.Substring(st, offset - st);
                                    else
                                        activities = CompanyInfoSection.Substring(0, CompanyInfoSection.LastIndexOf('.'));
                                }
                                else
                                {
                                    string sub = CompanyInfoSection.Substring(st, offset - st);

                                    /*  int sta = sub.IndexOf("(Objecto");
                                      int off = sub.IndexOf("objecto");

                                      string preString = "(Objecto";
                                      string searchString = "objecto";
                                      int preIndex = sub.IndexOf(preString);
                                      int searchIndex = preIndex + sub.Substring(preIndex).IndexOf(searchString);
                                      string cap = sub.Substring(preIndex, searchIndex - preIndex);
                                      int dot = sub.IndexOf("\n");  */
                                    string preStrin = "objecto";
                                    string searchStrin = ".";

                                    int preInde = sub.IndexOf(preStrin) + preStrin.Length;
                                    int searchInde = preInde + sub.Substring(preInde).IndexOf(searchStrin) + 1;

                                    activities = sub.Substring(preInde, searchInde - preInde);
                                }
                            }
                            //Step-21:
                            if (CompanyInfoSection.Contains("quotas"))
                            {                                
                                int st = CompanyInfoSection.IndexOf("ARTIGO") + 5;
                                int offset = CompanyInfoSection.LastIndexOf("ARTIGO") + 20;
                                if(st==4)
                                {
                                    st = CompanyInfoSection.IndexOf("quotas");
                                    owner = CompanyInfoSection.Substring(st);
                                }
                                else
                                { 
                                string sub = CompanyInfoSection.Substring(st, offset - st);
                                int offset1 = sub.IndexOf("quotas");

                                /*  int sta = sub.IndexOf("(Objecto");
                                  int off = sub.IndexOf("Objecto");
  */
                                for(int h=1;h<10;h++)
                                {
                                    offset1 = sub.IndexOf("quotas", offset1 + 1);
                                    string preString = "quota";
                                    string searchStr;
                                    Match match = Regex.Match(sub, "(Kz|KzR|Kz:)");
                                        if (match.Success)
                                        {
                                            searchStr = match.Value;


                                            //string searchString = searchStr;
                                            //int preIndex = sub.IndexOf(preString);
                                            //int searchIndex = preIndex + sub.Substring(preIndex).IndexOf(searchString);
                                            //string cap = sub.Substring(preIndex, searchIndex - preIndex);
                                            //int dot = sub.IndexOf("\n");
                                            string preStrin = "Kz";
                                            string searchStrin = "(";
                                            int preInde = sub.IndexOf(preStrin) + 4;
                                            int searchInde = preInde + sub.Substring(preInde).IndexOf(searchStrin);

                                            ownerholds = sub.Substring(preInde, searchInde - preInde);
                                            preStrin = sub.Substring(sub.IndexOf(ownerholds));
                                            preInde = preStrin.IndexOf("sócio");
                                            if (!(preInde == -1))
                                            {
                                                string newstrin = preStrin.Substring(preInde);
                                                searchStrin = ".";
                                                int startin = newstrin.IndexOf("sócio") + 5;
                                                int endin = newstrin.IndexOf(searchStrin);
                                                if (endin == -1)
                                                {
                                                    searchStrin = ",";
                                                    endin = newstrin.IndexOf(searchStrin);
                                                }
                                                owner = newstrin.Substring(startin, endin - startin);
                                            }
                                        }

                                    }
                                }                               
                            }                               
                            if (CompanyInfoSection.Contains(shareholder))
                            {
                                int s = CompanyInfoSection.IndexOf(shareholder);
                                shareinfo = CompanyInfoSection.Substring(s);
                                if (shareinfo.Contains("titular"))
                                {
                                    int st = shareinfo.IndexOf("titular");
                                    string t = shareinfo.Substring(st);
                                    if (t.Contains("n.º"))
                                    {
                                        int stin = t.IndexOf("n.º") + 3;
                                        t = t.Substring(stin);
                                        int en = t.IndexOf(',');
                                        id = t.Substring(0, en);
                                    }
                                    else
                                    {
                                        Match match = Regex.Match(t, "\\s[0-9][0-9]*");
                                        if (match.Success)
                                        {
                                            id = match.Captures[0].Value; // Will output only num
                                        }
                                    }
                                }
                                if (shareinfo.Contains("portador"))
                                {
                                    int st = shareinfo.IndexOf("portador");
                                    string t = shareinfo.Substring(st);
                                    if (t.Contains("n.º"))
                                    {
                                        int stin = t.IndexOf("n.º") + 3;
                                        t = t.Substring(stin);
                                        int en = t.IndexOf(',');
                                        id = t.Substring(0, en);
                                    }
                                    else
                                    {
                                        Match match = Regex.Match(t, "\\s[0-9][0-9]*");
                                        if (match.Success)
                                        {
                                            id = match.Captures[0].Value; // Will output only num
                                        }
                                    }
                                }
                                if (shareinfo.Contains("contribuinte fiscal"))
                                {
                                    int st = shareinfo.IndexOf("contribuinte fiscal");
                                    string t = shareinfo.Substring(st);
                                    if (t.Contains("n.º"))
                                    {
                                        int stin = t.IndexOf("n.º") + 3;
                                        t = t.Substring(stin);
                                        int en = t.IndexOf(',');
                                        id = t.Substring(0, en);
                                    }
                                    else
                                    {
                                        Match match = Regex.Match(t, "\\s[0-9][0-9]*");
                                        if (match.Success)
                                        {
                                            id = match.Captures[0].Value; // Will output only num
                                        }
                                    }
                                }
                            }
                            //Step-9:
                            if (CompanyInfoSection.Contains(shareholder1))
                            {
                                int s = CompanyInfoSection.IndexOf(shareholder1);
                                shareinfo1 = CompanyInfoSection.Substring(s);
                                if (shareinfo1.Contains("titular"))
                                {
                                    int st = shareinfo1.IndexOf("titular");
                                    string t = shareinfo1.Substring(st);
                                    if (t.Contains("n.º"))
                                    {
                                        int stin = t.IndexOf("n.º") + 3;
                                        t = t.Substring(stin);
                                        int en = t.IndexOf(',');
                                        id = t.Substring(0, en);
                                    }
                                    else
                                    {
                                        Match match = Regex.Match(t, "\\s[0-9][0-9]*");
                                        if (match.Success)
                                        {
                                            id = match.Captures[0].Value; // Will output only num
                                        }
                                    }
                                }
                                if (shareinfo1.Contains("portador"))
                                {
                                    int st = shareinfo1.IndexOf("portador");
                                    string t = shareinfo1.Substring(st);
                                    if (t.Contains("n.º"))
                                    {
                                        int stin = t.IndexOf("n.º") + 3;
                                        t = t.Substring(stin);
                                        int en = t.IndexOf(',');
                                        id = t.Substring(0, en);
                                    }
                                    else
                                    {
                                        Match match = Regex.Match(t, "\\s[0-9][0-9]*");
                                        if (match.Success)
                                        {
                                            id = match.Captures[0].Value; // Will output only num
                                        }
                                    }
                                }
                                if (shareinfo1.Contains("contribuinte fiscal"))
                                {
                                    int st = shareinfo1.IndexOf("contribuinte fiscal");
                                    string t = shareinfo1.Substring(st);
                                    if (t.Contains("n.º"))
                                    {
                                        int stin = t.IndexOf("n.º") + 3;
                                        t = t.Substring(stin);
                                        int en = t.IndexOf(',');
                                        id = t.Substring(0, en);
                                    }
                                    else
                                    {
                                        Match match = Regex.Match(t, "\\s[0-9][0-9]*");
                                        if (match.Success)
                                        {
                                            id = match.Captures[0].Value; // Will output only num
                                        }
                                    }
                                }
                            }
                            //Step-9:
                            if (CompanyInfoSection.Contains(shareholder2))
                            {
                                int s = CompanyInfoSection.IndexOf(shareholder2);
                                shareinfo2 = CompanyInfoSection.Substring(s);
                                if (shareinfo2.Contains("titular"))
                                {
                                    int st = shareinfo2.IndexOf("titular");
                                    string t = shareinfo2.Substring(st);
                                    if (t.Contains("n.º"))
                                    {
                                        int stin = t.IndexOf("n.º") + 3;
                                        t = t.Substring(stin);
                                        int en = t.IndexOf(',');
                                        id = t.Substring(0, en);
                                    }
                                    else
                                    {
                                        Match match = Regex.Match(t, "\\s[0-9][0-9]*");
                                        if (match.Success)
                                        {
                                            id = match.Captures[0].Value; // Will output only num
                                        }
                                    }
                                }
                                if (shareinfo2.Contains("portador"))
                                {
                                    int st = shareinfo2.IndexOf("portador");
                                    string t = shareinfo2.Substring(st);
                                    if (t.Contains("n.º"))
                                    {
                                        int stin = t.IndexOf("n.º") + 3;
                                        t = t.Substring(stin);
                                        int en = t.IndexOf(',');
                                        id = t.Substring(0, en);
                                    }
                                    else
                                    {
                                        Match match = Regex.Match(t, "\\s[0-9][0-9]*");
                                        if (match.Success)
                                        {
                                            id = match.Captures[0].Value; // Will output only num
                                        }
                                    }
                                }
                                if (shareinfo2.Contains("contribuinte fiscal"))
                                {
                                    int st = shareinfo2.IndexOf("contribuinte fiscal");
                                    string t = shareinfo2.Substring(st);
                                    if (t.Contains("n.º"))
                                    {
                                        int stin = t.IndexOf("n.º") + 3;
                                        t = t.Substring(stin);
                                        int en = t.IndexOf(',');
                                        id = t.Substring(0, en);
                                    }
                                    else
                                    {
                                        Match match = Regex.Match(t, "\\s[0-9][0-9]*");
                                        if (match.Success)
                                        {
                                            id = match.Captures[0].Value; // Will output only num
                                        }
                                    }
                                }
                            }

                            /* Console.WriteLine(indexline);
                             Console.WriteLine(CompanyInfoSection);
                             Console.WriteLine(doi);
                             Console.WriteLine(shareholder);
                             Console.WriteLine(address);
                             Console.WriteLine(id);
                             Console.WriteLine(shareholder1);
                             Console.WriteLine(birthplace);
                             Console.WriteLine(address1);
                             Console.WriteLine(shareholder2);
                             Console.WriteLine(address2);
                             Console.WriteLine(shareholder3);
                             Console.WriteLine(address3);
                             Console.WriteLine(headquarters);
                             Console.WriteLine(companynum);
                             Console.WriteLine(activities);   */
  
                            string shareholderdetail = "Name - "+shareholder +"\n"+"Address - "+ address+"\n" +"Nationality - "+ nationality+"\n" +"ID - "+ id+"\n" +"PlaceofBirth - "+ birthplace;
                            string shareholderdetail1 = "Name - " + shareholder1 + "\n" + "Address - " + address1 + "\n" + "Nationality - " + nationality + "\n" + "ID - " + id + "\n" + "PlaceofBirth - " + birthplace1;
                            string shareholderdetail2 = "Name - " + shareholder2 + "\n" + "Address - " + address2 + "\n" + "Nationality - " + nationality + "\n" + "ID - " + id + "\n" + "PlaceofBirth - " + birthplace2; 
                            if (ScrapingDone)
                                break;
                           
                        }
                        else
                        {
                            if (documentline.ToLower().Contains(indexline.ToLower().Substring(0, (indexline.Length / 2))))
                            {
                                if (indexline.ToLower().Contains(documentline.ToLower() + document[i + 1].Trim().Replace(" ", "").ToLower()))
                                {
                                    for (int j = i + 1; j < document.Length; j++)
                                    {
                                        string doccontent = document[j].Trim().Replace(" ", "");
                                        if (!(doccontent.Contains("Oajudante")))
                                            CompanyInfoSection = CompanyInfoSection + document[j];
                                        else
                                        {
                                            CompanyInfoSection = CompanyInfoSection + document[j];
                                            ScrapingDone = true;
                                            break;
                                        }
                                        if (doccontent.Contains("constituída") || doccontent.Contains("constituida") || doccontent.Contains("contituição") || doccontent.Contains("contituem") || doccontent.Contains("constitue"))
                                        {
                                            string tempd = doi + document[j - 1] + document[j] + document[j + 1];
                                            int st = tempd.IndexOf(',');
                                            int en = tempd.LastIndexOf(',');
                                            if (!(en == -1))
                                            {
                                                doi = tempd.Substring(st, en - st);
                                                Match match = Regex.Match(doi, "[0-9]+[^0-9]+[0-9][0-9][0-9][0-9]");
                                                if (match.Success)
                                                {
                                                    doi = match.Captures[0].Value; // Will output only date
                                                }
                                            }
                                        }

                                        if (doccontent.Contains("casado") || doccontent.Contains("casada") || doccontent.Contains("solteiro") || doccontent.Contains("solteira"))
                                        {
                                            string temps = shareholder + document[j];
                                            int e = temps.IndexOf(',');
                                            if (!(e == -1))
                                            {
                                                shareholder = temps.Substring(0, e);
                                            }

                                            /*if (doccontent.Contains("naturalidade") || doccontent.Contains("natural") || doccontent.Contains("natural de") || doccontent.Contains("natural da") || doccontent.Contains("natural do"))
                                            {
                                                string tempb = birthplace + document[j];
                                                int st = tempb.IndexOf("natural");
                                                int en = tempb.LastIndexOf(',');
                                                if (!(en == 1))
                                                {
                                                    birthplace = tempb.Substring(st, en - st);
                                                }
                                            }*/
                                        }
                                    }
                                    if (ScrapingDone)
                                        break;
                                }
                            }
                        }

                    }
                    //context.companyDetails.Add(company);
                    //context.SaveChanges();
                    if (!d.ContainsKey(indexline))
                        d.Add(new KeyValuePair<string, string>(indexline, CompanyInfoSection));
                    else
                    {
                        d[indexline] = d[indexline] + CompanyInfoSection;
                    }
                    if (!date.ContainsKey(indexline))
                        date.Add(new KeyValuePair<string, string>(indexline, doi));
                    else
                    {
                        date[indexline] = date[indexline] + doi;
                    }
                    if (!share.ContainsKey(indexline))
                        share.Add(new KeyValuePair<string, string>(indexline, shareholder));
                    else
                    {
                        share[indexline] = share[indexline] + shareholder;
                    }                    
                    if (!share1.ContainsKey(indexline))
                        share1.Add(new KeyValuePair<string, string>(indexline, shareholder1));
                    else
                    {
                        share1[indexline] = share1[indexline] + shareholder1;
                    }

                    company.DateOfIncorporation = doi;
                    company.NameOfCompany = line;
                    company.Owner = owner;
                    company.OwnerHolds = ownerholds;
                    company.Activities = activities;
                    company.CompanyNumber = companynum;
                    company.Capital = capital;
                    company.Headquarters = headquarters;

                }                

                //else
                //{
                //    foreach (var newline in readlines)
                //    {
                //        string indexline = newline.Remove(line.Length - 2, 2).Trim().Replace(" ", "");

                //        if (!(indexline.Contains("Limitada")))
                //        {

                //        }
                //        for (int i = 0; i < document.Length; i++)
                //        {
                //            string documentline = document[i].Trim().Replace(" ", "");
                //        }
                //    }
                //}

                //string newline = line.Remove(line.Length - 2, 2);

                //if(newline.Contains("Umitada")||newline.Contains("limitada")||newline.Contains("Limitada"))
                //{

                //    string f = Regex.Replace(detail, @"\s+", String.Empty);
                //    string g = Regex.Replace(newline, @"\s+", String.Empty);
                //    bool contains = Regex.IsMatch(f, Regex.Escape(g), RegexOptions.IgnoreCase);
                //    //string x = newline.Substring(0, 7);
                //    //if (detail.Contains(x))
                //    if (contains == true)
                //    {

                //        int s = f.IndexOf(g, StringComparison.CurrentCultureIgnoreCase);
                //        string companydetails = f.Substring(s);
                //        int e = companydetails.IndexOf("Oajudante");
                //        string cd = companydetails.Substring(0, e);
                //        //string n = Regex.Replace(cd, " ",String.Empty);
                //    }



                //    //detail.Any(c=>newline.Contains(c))
                //    //        {
                //    //        return null;
                //    //    }

                //}



                //int s = detail.IndexOf(newline);

                //string companydetails = detail.Substring(s);
                //int e = companydetails.IndexOf("O ajudante");
                //string cd = companydetails.Substring(0, e);
                
                context.companyDetails.Add(company);
                context.SaveChanges();

            }
            

            return txt;
        }
    }
}
