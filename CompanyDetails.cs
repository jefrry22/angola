﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Angola
{
    class CompanyDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string NameOfCompany { get; set; }
        public string DateOfIncorporation { get; set; }
        public string  Headquarters { get; set; }
        public string CompanyNumber { get; set; }
        public string Activities { get; set; }
        public string Capital { get; set; }
        public string Owner { get; set; }
        public string OwnerHolds { get; set; }
        public string Management { get; set; }


    }
}
