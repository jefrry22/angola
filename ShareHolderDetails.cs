﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Angola
{
    class ShareHolderDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string Shareholder1 { get; set; }
        public string Address1 { get; set; }
        public string Birthplace1 { get; set; }
        public string Nationality1 { get; set; }
        public string ID1 { get; set; }
        public string Shareholder2 { get; set; }
        public string Address2 { get; set; }
        public string Birthplace2 { get; set; }
        public string Nationality2 { get; set; }
        public string ID2 { get; set; }
        public string Shareholder3 { get; set; }
        public string Address3 { get; set; }
        public string Birthplace3 { get; set; }
        public string Nationality3 { get; set; }
        public string ID3 { get; set; }
        //public CompanyDetails details { get; set; }
    }
}
