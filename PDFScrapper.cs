﻿using org.apache.pdfbox.pdmodel;
using org.apache.pdfbox.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
namespace Angola
{
    class PDFScrapper
    {       

    public string testextract(string PDFFilePath)
        {
            ScrapeDbContext context = new ScrapeDbContext();
            CompanyDetails company = new CompanyDetails();
            ShareHolderDetails shareHolderDetails = new ShareHolderDetails();
            PDDocument doc = PDDocument.load(PDFFilePath);
            PDFTextStripper stripper = new PDFTextStripper();
            stripper.setStartPage(1);
            stripper.setEndPage(stripper.getEndPage());
            string txt = stripper.getText(doc);

            int start = txt.LastIndexOf("SUMÁRIO");
            int end = txt.LastIndexOf("Limitada».");
            string Companyname = txt.Substring(start, end - start).Replace("Umitada", "Limitada").Trim();
            string[] readlines = Companyname.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);



            PDFTextStripper docforIndex = new PDFTextStripper();
            string details = docforIndex.getText(doc).Trim();
            string[] documents = details.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            List<string> Companylist = new List<string>();
            string EndIndex = "";
            int startIndex = 0;
            for (int i = 0; i < documents.Length; i++)
            {
                if (documents[i].Contains("SUMÁRIO"))
                {
                    startIndex = i;
                    break;
                }
            }
            for (int i = startIndex; i < documents.Length; i++)
            {
                if (documents[i].ToLower().Contains("limitada") || documents[i].ToLower().Contains("umitada"))
                {
                    if ((Companylist.Count != 0) && (documents[i].ToLower().Trim() != EndIndex))
                        Companylist.Add(documents[i]);
                    else
                    {
                        if (documents[i].ToLower().Trim() == EndIndex)
                            break;
                        if (Companylist.Count == 0)
                            EndIndex = documents[i].ToLower().Trim();
                        Companylist.Add(documents[i]);
                    }
                }
                else if (i < documents.Length - 1)
                {
                    if (documents[i + 1].ToLower().Contains("limitada") || documents[i + 1].ToLower().Contains("umitada"))
                    {
                        if ((Companylist.Count != 0) && ((documents[i].ToLower().TrimStart() + documents[i + 1].ToLower().TrimEnd()) != EndIndex))
                        {
                            Companylist.Add(documents[i].ToLower().TrimStart() + documents[i + 1].ToLower().TrimEnd());
                            i++;
                        }
                        else
                        {
                            if ((documents[i].ToLower().TrimStart() + documents[i + 1].ToLower().TrimEnd()) == EndIndex)
                                break;
                            if (Companylist.Count == 0)
                                EndIndex = documents[i].ToLower().TrimStart() + documents[i + 1].ToLower().TrimEnd();
                            Companylist.Add(documents[i].ToLower().TrimStart() + documents[i + 1].ToLower().TrimEnd());
                            i++;
                        }
                    }
                }
            }
             
            for (int i = 0; i < Companylist.Count; i++)
            {
                string companyinfo = "";
                string doi = "";
                string shareholder = "";
                string shareholder1 = "";
                string shareholder2 = "";
                string shareholder3 = "";
                string birthplace = "";
                string birthplace1 = "";
                string birthplace2 = "";
                string birthplace3 = "";
                string address = "";
                string address1 = "";
                string address2 = "";
                string address3 = "";
                string nationality1 = "";
                string nationality2 = "";
                string nationality3 = "";
                string id1 = "";
                string id2 = "";
                string id3 = "";
                string headquarters = "";
                string activities = "";
                string capital = "";
                string owner = "";
                string ownerholds = "";
                string companynumber = "";
                string management = "";
                if (txt.Contains(Companylist[i]))
                {
                    string indexline = Companylist[i].Remove(Companylist[i].Length - 2, 2);
                    //string newtxt = txt.Replace(" ", "");                
                    int n = txt.IndexOf(indexline);
                    int l = indexline.Length;

                    string[] ssize = indexline.Split(null);
                    ////  int ni = PartialMatching.Compute(indexline, );


                    string sample = txt.Substring(n + l);
                    int x = 1;
                    int index = txt.IndexOf(ssize[x]);


 //-----------------------------------------Get Company Info-------------------------------------------------------//                   
                    if (sample.ToLower().Trim().Contains(indexline.ToLower().Trim()))
                    {
                        int newn = sample.ToLower().IndexOf(indexline.ToLower());

                        string newsamp = sample.Substring(newn);
                        int endn = newsamp.IndexOf(Companylist[i + 1].Remove(Companylist[i + 1].Length / 2));
                        if (!(endn == -1))
                        {   
                            companyinfo = newsamp.Substring(0, endn);
                        }
                        else
                        {
                            endn = newsamp.IndexOf("Ce1tifica");
                            if (!(endn == -1))
                            {
                                companyinfo = newsamp.Substring(0, endn);
                            }
                            else
                            {
                                endn = newsamp.IndexOf("Ce1tidão");
                                if (!(endn == -1))
                                {
                                    companyinfo = newsamp.Substring(0, endn);
                                }
                                else
                                {
                                    endn = newsamp.IndexOf("CERTIDÃO");
                                    if (!(endn == -1))
                                    {
                                        companyinfo = newsamp.Substring(0, endn);
                                    }
                                    else
                                        endn = newsamp.IndexOf("Ce1tifico");
                                    companyinfo = newsamp.Substring(0, endn);
                                }
                            }

                        }
 //--------------------------------------------------- Date Of Incorporation-------------------------------------//

                        if (companyinfo.Contains("Constituição"))
                        {
                            int s = companyinfo.IndexOf("Constituição");
                            doi = companyinfo.Substring(s);
                            Match match = Regex.Match(doi, "[0-9]+[^0-9]+[0-9][0-9][0-9][0-9]");
                            if (match.Success)
                            {
                                doi = match.Captures[0].Value; // Will output only date

                            }
                        }
                        else if (companyinfo.Contains("constituida"))
                        {
                            int s = companyinfo.IndexOf("constituida");
                            doi = companyinfo.Substring(s);
                            Match match = Regex.Match(doi, "[0-9]+[^0-9]+[0-9][0-9][0-9][0-9]");
                            if (match.Success)
                            {
                                doi = match.Captures[0].Value; // Will output only date

                            }
                        }
                        else if (companyinfo.Contains("constituída"))
                        {
                            int s = companyinfo.IndexOf("constituída");
                            doi = companyinfo.Substring(s);
                            Match match = Regex.Match(doi, "[0-9]+[^0-9]+[0-9][0-9][0-9][0-9]");
                            if (match.Success)
                            {
                                doi = match.Captures[0].Value; // Will output only date

                            }
                        }
                        else if (companyinfo.Contains("contituem"))
                        {
                            int s = companyinfo.IndexOf("contituem");
                            doi = companyinfo.Substring(s);
                            Match match = Regex.Match(doi, "[0-9]+[^0-9]+[0-9][0-9][0-9][0-9]");
                            if (match.Success)
                            {
                                doi = match.Captures[0].Value; // Will output only date

                            }
                        }
                        else if (companyinfo.Contains("constitue"))
                        {
                            int s = companyinfo.IndexOf("constitue");
                            doi = companyinfo.Substring(s);
                            Match match = Regex.Match(doi, "[0-9]+[^0-9]+[0-9][0-9][0-9][0-9]");
                            if (match.Success)
                            {
                                doi = match.Captures[0].Value; // Will output only date

                            }
                        }
                        else if (companyinfo.Contains("contituição"))
                        {
                            int s = companyinfo.IndexOf("contituição");
                            doi = companyinfo.Substring(s);
                            Match match = Regex.Match(doi, "[0-9]+[^0-9]+[0-9][0-9][0-9][0-9]");
                            if (match.Success)
                            {
                                doi = match.Captures[0].Value; // Will output only date

                            }
                        }
 //---------------------------------------------Get Shareholder details-------------------------------------------//

                        string shareinfo = companyinfo.Substring(companyinfo.IndexOf(doi));
                        if (companyinfo.Contains("solteira") || companyinfo.Contains("solteiro") || companyinfo.Contains("casada") || companyinfo.Contains("casado"))
                        {
                            string temps = companyinfo;
                            if (temps.Contains("Primeiro") || temps.Contains("Primeira"))
                            {
                                int s = temps.IndexOf("Primeiro:") + 9;
                                if ((s == 8))
                                {
                                    s = temps.IndexOf("Primeira:") + 9;
                                    shareholder1 = temps.Substring(s);
                                    Match match = Regex.Match(shareholder1, "[A-Z].*");
                                    if (match.Success)
                                    {
                                        shareholder1 = match.Value;
                                        if(shareholder1.Contains(','))
                                        shareholder1 = shareholder1.Substring(0, shareholder1.IndexOf(','));
                                    }
                                }
                                shareholder1 = temps.Substring(s);
                                Match match1 = Regex.Match(shareholder1, "[A-Z].*");
                                if (match1.Success)
                                {
                                    shareholder1 = match1.Value;
                                    if(shareholder1.Contains(','))
                                    shareholder1 = shareholder1.Substring(0, shareholder1.IndexOf(','));
                                }
                                string tempbirth = companyinfo.Substring(companyinfo.IndexOf(shareholder1));
                                if (tempbirth.Contains("natural"))
                                {
                                    birthplace1 = tempbirth.Substring(tempbirth.IndexOf("natural"));
                                    Match match = Regex.Match(birthplace1, "\\b([A-Z][a-z]*)");
                                    if (match.Success)
                                    {
                                        birthplace1 = match.Value;

                                    }
                                }
                                string tempaddress = companyinfo.Substring(companyinfo.IndexOf(shareholder1));
                                if (tempaddress.Contains("residente"))
                                {
                                    address1 = tempaddress.Substring(tempaddress.IndexOf("residente"));
                                    Match match = Regex.Match(address1, "([A-Z][a-z']*)");
                                    if (match.Success)
                                    {
                                        address1 = match.Value;
                                    }
                                }
                                string tempnational = companyinfo.Substring(companyinfo.IndexOf(shareholder1));
                                if (tempnational.Contains("nacionalidade"))
                                {
                                    int st = tempnational.IndexOf("nacionalidade") + 13;
                                    nationality1 = tempnational.Substring(st);
                                    int e = nationality1.IndexOf(',');
                                    nationality1 = tempnational.Substring(0, e);
                                }
                                string tempid = companyinfo.Substring(companyinfo.IndexOf(shareholder1));
                                if (tempid.Contains("titular"))
                                {
                                    int st = tempid.IndexOf("titular");
                                    string t = tempid.Substring(st);
                                    if (t.Contains("n.º"))
                                    {
                                        int stin = t.IndexOf("n.º") + 3;
                                        t = t.Substring(stin);
                                        int en = t.IndexOf(',');
                                        id1 = t.Substring(0, en);
                                    }
                                    else
                                    {
                                        Match match = Regex.Match(t, "\\s[0-9][0-9]*");
                                        if (match.Success)
                                        {
                                            id1 = match.Captures[0].Value; // Will output only num
                                        }
                                    }
                                }
                                if (tempid.Contains("portador"))
                                {
                                    int st = tempid.IndexOf("portador");
                                    string t = tempid.Substring(st);
                                    if (t.Contains("n.º"))
                                    {
                                        int stin = t.IndexOf("n.º") + 3;
                                        t = t.Substring(stin);
                                        int en = t.IndexOf(',');
                                        id3 = t.Substring(0, en);
                                    }
                                    else
                                    {
                                        Match match = Regex.Match(t, "\\s[0-9][0-9]*");
                                        if (match.Success)
                                        {
                                            id1 = match.Captures[0].Value; // Will output only num
                                        }
                                    }
                                }
                                if (tempid.Contains("contribuinte fiscal"))
                                {
                                    int st = tempid.IndexOf("contribuinte fiscal");
                                    string t = tempid.Substring(st);
                                    if (t.Contains("n.º"))
                                    {
                                        int stin = t.IndexOf("n.º") + 3;
                                        t = t.Substring(stin);
                                        int en = t.IndexOf(',');
                                        id1 = t.Substring(0, en);
                                    }
                                    else
                                    {
                                        Match match = Regex.Match(t, "\\s[0-9][0-9]*");
                                        if (match.Success)
                                        {
                                            id1 = match.Captures[0].Value; // Will output only num
                                        }
                                    }
                                }
                            }                                                                
                            if (temps.Contains("Segundo"))
                            {
                                int st = temps.IndexOf("Segundo:") + 7;
                                shareholder2 = temps.Substring(st);
                                Match match = Regex.Match(shareholder2, "[A-Z].*");
                                if (match.Success)
                                {
                                    shareholder2 = match.Value;
                                    if(shareholder2.Contains(','))
                                    shareholder2 = shareholder2.Substring(0, shareholder2.IndexOf(','));
                                }
                                string tempbirth = companyinfo.Substring(companyinfo.IndexOf(shareholder2));
                                if (tempbirth.Contains("natural"))
                                {
                                    birthplace2 = tempbirth.Substring(tempbirth.IndexOf("natural"));
                                    match = Regex.Match(birthplace2, "\\b([A-Z][a-z]*)");
                                    if (match.Success)
                                    {
                                        birthplace2 = match.Value;

                                    }
                                }
                                string tempaddress = companyinfo.Substring(companyinfo.IndexOf(shareholder2));
                                if (tempaddress.Contains("residente"))
                                {
                                    address2 = tempaddress.Substring(tempaddress.IndexOf("residente"));
                                    match = Regex.Match(address2, "([A-Z][a-z']*)");
                                    if (match.Success)
                                    {
                                        address2 = match.Value;
                                    }
                                }
                                string tempnational = companyinfo.Substring(companyinfo.IndexOf(shareholder2));
                                if (tempnational.Contains("nacionalidade"))
                                {
                                    int s = tempnational.IndexOf("nacionalidade") + 13;
                                    nationality2 = tempnational.Substring(s);
                                    int e = nationality2.IndexOf(',');
                                    nationality2 = tempnational.Substring(0, e);
                                }
                                string tempid = companyinfo.Substring(companyinfo.IndexOf(shareholder2));
                                if (tempid.Contains("titular"))
                                {
                                    int s = tempid.IndexOf("titular");
                                    string t = tempid.Substring(s);
                                    if (t.Contains("n.º"))
                                    {
                                        int stin = t.IndexOf("n.º") + 3;
                                        t = t.Substring(stin);
                                        int en = t.IndexOf(',');
                                        id2 = t.Substring(0, en);
                                    }
                                    else
                                    {
                                        match = Regex.Match(t, "\\s[0-9][0-9]*");
                                        if (match.Success)
                                        {
                                            id2 = match.Captures[0].Value; // Will output only num
                                        }
                                    }
                                }
                                if (tempid.Contains("portador"))
                                {
                                    int s = tempid.IndexOf("portador");
                                    string t = tempid.Substring(s);
                                    if (t.Contains("n.º"))
                                    {
                                        int stin = t.IndexOf("n.º") + 3;
                                        t = t.Substring(stin);
                                        int en = t.IndexOf(',');
                                        id2 = t.Substring(0, en);
                                    }
                                    else
                                    {
                                        match = Regex.Match(t, "\\s[0-9][0-9]*");
                                        if (match.Success)
                                        {
                                            id2 = match.Captures[0].Value; // Will output only num
                                        }
                                    }
                                }
                                if (tempid.Contains("contribuinte fiscal"))
                                {
                                    int s = tempid.IndexOf("contribuinte fiscal");
                                    string t = tempid.Substring(s);
                                    if (t.Contains("n.º"))
                                    {
                                        int stin = t.IndexOf("n.º") + 3;
                                        t = t.Substring(stin);
                                        int en = t.IndexOf(',');
                                        id2 = t.Substring(0, en);
                                    }
                                    else
                                    {
                                        match = Regex.Match(t, "\\s[0-9][0-9]*");
                                        if (match.Success)
                                        {
                                            id2 = match.Captures[0].Value; // Will output only num
                                        }
                                    }
                                }
                            }
                            if (temps.Contains("Terceiro"))
                            {
                                int s = temps.IndexOf("Terceiro:") + 9;
                                shareholder3 = temps.Substring(s);
                                Match match = Regex.Match(shareholder3, "[A-Z].*");
                                if (match.Success)
                                {
                                    shareholder3 = match.Value;
                                    if (shareholder3.Contains(','))
                                    shareholder3 = shareholder3.Substring(0, shareholder3.IndexOf(','));
                                }
                                string tempbirth = companyinfo.Substring(companyinfo.IndexOf(shareholder3));
                                if (tempbirth.Contains("natural"))
                                {
                                    birthplace3 = tempbirth.Substring(tempbirth.IndexOf("natural"));
                                    match = Regex.Match(birthplace3, "\\b([A-Z][a-z]*)");
                                    if (match.Success)
                                    {
                                        birthplace3 = match.Value;

                                    }
                                }
                                string tempaddress = companyinfo.Substring(companyinfo.IndexOf(shareholder3));
                                if (tempaddress.Contains("residente"))
                                {
                                    address3 = tempaddress.Substring(tempaddress.IndexOf("residente"));
                                    match = Regex.Match(address3, "([A-Z][a-z']*)");
                                    if (match.Success)
                                    {
                                        address3 = match.Value;
                                    }
                                }
                                string tempnational = companyinfo.Substring(companyinfo.IndexOf(shareholder3));
                                if (tempnational.Contains("nacionalidade"))
                                {
                                    int st = tempnational.IndexOf("nacionalidade") + 13;
                                    nationality3 = tempnational.Substring(st);
                                    int e = nationality3.IndexOf(',');
                                    nationality3 = tempnational.Substring(0, e);
                                }
                                string tempid = companyinfo.Substring(companyinfo.IndexOf(shareholder3));
                                if (tempid.Contains("titular"))
                                {
                                    int st = tempid.IndexOf("titular");
                                    string t = tempid.Substring(st);
                                    if (t.Contains("n.º"))
                                    {
                                        int stin = t.IndexOf("n.º") + 3;
                                        t = t.Substring(stin);
                                        int en = t.IndexOf(',');
                                        id3 = t.Substring(0, en);
                                    }
                                    else
                                    {
                                        match = Regex.Match(t, "\\s[0-9][0-9]*");
                                        if (match.Success)
                                        {
                                            id3 = match.Captures[0].Value; // Will output only num
                                        }
                                    }
                                }
                                if (tempid.Contains("portador"))
                                {
                                    int st = tempid.IndexOf("portador");
                                    string t = tempid.Substring(st);
                                    if (t.Contains("n.º"))
                                    {
                                        int stin = t.IndexOf("n.º") + 3;
                                        t = t.Substring(stin);
                                        int en = t.IndexOf(',');
                                        id3 = t.Substring(0, en);
                                    }
                                    else
                                    {
                                        match = Regex.Match(t, "\\s[0-9][0-9]*");
                                        if (match.Success)
                                        {
                                            id3 = match.Captures[0].Value; // Will output only num
                                        }
                                    }
                                }
                                if (tempid.Contains("contribuinte fiscal"))
                                {
                                    int st = tempid.IndexOf("contribuinte fiscal");
                                    string t = tempid.Substring(st);
                                    if (t.Contains("n.º"))
                                    {
                                        int stin = t.IndexOf("n.º") + 3;
                                        t = t.Substring(stin);
                                        int en = t.IndexOf(',');
                                        id3 = t.Substring(0, en);
                                    }
                                    else
                                    {
                                        match = Regex.Match(t, "\\s[0-9][0-9]*");
                                        if (match.Success)
                                        {
                                            id3 = match.Captures[0].Value; // Will output only num
                                        }
                                    }
                                }
                            }
                        }
//------------------------------------------Step-17 - HEADQUARTERS---------------------------------------------------//

                        if (companyinfo.Contains("sede"))
                        {
                            int st = companyinfo.IndexOf("ARTIGO") + 5;
                            int offset = companyinfo.LastIndexOf("ARTIGO") + 20;
                            if (st == 4)
                            {
                                st = companyinfo.IndexOf("sede");
                                offset = companyinfo.IndexOf("Objecto:");
                                if (!(offset == -1)&&offset>st)
                                    headquarters = companyinfo.Substring(st, offset - st);
                            }
                            else
                            {
                                string sub = companyinfo.Substring(st, offset - st);
                                string preStrin = "sede";
                                string searchStrin = ".";
                                int preInde = sub.IndexOf(preStrin) + 4;
                                int searchInde = preInde + sub.Substring(preInde).IndexOf(searchStrin) + 1;

                                headquarters = sub.Substring(preInde, searchInde - preInde);
                            }
                        }
//----------------------------------------------Step-18- COMPANY NUMBER-------------------------------------------//

                        if(companyinfo.Contains("N.I.F."))
                        {
                            string tempcompnum = companyinfo.Substring(companyinfo.IndexOf("N.I.F."));
                            Match match = Regex.Match(tempcompnum, "\\s[0-9][0-9]*");
                            if (match.Success)
                            {
                                companynumber = match.Captures[0].Value; // Will output only num
                            }
                        }
                        else if(companyinfo.Contains("contribuinte fiscal"))
                        {
                            string tempcompnum = companyinfo.Substring(companyinfo.IndexOf("contribuinte fiscal"));
                            Match match = Regex.Match(tempcompnum, "\\s[0-9][0-9]*");
                            if (match.Success)
                            {
                                companynumber = match.Captures[0].Value; // Will output only num
                            }
                        }
                        else if(companyinfo.Contains("Contribuinte Fiscal"))
                        {
                            string tempcompnum = companyinfo.Substring(companyinfo.IndexOf("Contribuinte Fiscal"));
                            Match match = Regex.Match(tempcompnum, "\\s[0-9][0-9]*");
                            if (match.Success)
                            {
                                companynumber = match.Captures[0].Value; // Will output only num
                            }
                        }
                        else if(companyinfo.Contains("NIF."))
                        {
                            string tempcompnum = companyinfo.Substring(companyinfo.IndexOf("NIF."));
                            Match match = Regex.Match(tempcompnum, "\\s[0-9][0-9]*");
                            if (match.Success)
                            {
                                companynumber = match.Captures[0].Value; // Will output only num
                            }
                        }
                        else if(companyinfo.Contains("NIF"))
                        {
                            string tempcompnum = companyinfo.Substring(companyinfo.IndexOf("NIF"));
                            Match match = Regex.Match(tempcompnum, "\\s[0-9][0-9]*");
                            if (match.Success)
                            {
                                companynumber = match.Captures[0].Value; // Will output only num
                            }
                        }
                        else if(companyinfo.Contains("NIF:"))
                        {
                            string tempcompnum = companyinfo.Substring(companyinfo.IndexOf("NIF:"));
                            Match match = Regex.Match(tempcompnum, "\\s[0-9][0-9]*");
                            if (match.Success)
                            {
                                companynumber = match.Captures[0].Value; // Will output only num
                            }
                        }
                        else if(companyinfo.Contains("N.I.F.:"))
                        {
                            string tempcompnum = companyinfo.Substring(companyinfo.IndexOf("N.I.F.:"));
                            Match match = Regex.Match(tempcompnum, "\\s[0-9][0-9]*");
                            if (match.Success)
                            {
                                companynumber = match.Captures[0].Value; // Will output only num
                            }
                        }
                        else if(companyinfo.Contains("Numero de Identificacao Fiscal"))
                        {
                            string tempcompnum = companyinfo.Substring(companyinfo.IndexOf("Numero de Identificacao Fiscal"));
                            Match match = Regex.Match(tempcompnum, "\\s[0-9][0-9]*");
                            if (match.Success)
                            {
                                companynumber = match.Captures[0].Value; // Will output only num
                            }
                        }
                        else if(companyinfo.Contains("Número de Identificação Fiscal"))
                        {
                            string tempcompnum = companyinfo.Substring(companyinfo.IndexOf("Número de Identificação Fiscal"));
                            Match match = Regex.Match(tempcompnum, "\\s[0-9][0-9]*");
                            if (match.Success)
                            {
                                companynumber = match.Captures[0].Value; // Will output only num
                            }
                        }
                        else if(companyinfo.Contains("matriculada"))
                        {
                            string tempcompnum = companyinfo.Substring(companyinfo.IndexOf("matriculada"));
                            Match match = Regex.Match(tempcompnum, "\\s[0-9][0-9]*");
                            if (match.Success)
                            {
                                companynumber = match.Captures[0].Value; // Will output only num
                            }
                        }
//----------------------------------------------Step-20- CAPITAL---------------------------------------------------//

                        if(companyinfo.ToLower().Contains("capital"))
                        {
                            string tempcap = companyinfo.ToLower().Substring(companyinfo.ToLower().IndexOf("capital"));
                            Match match = Regex.Match(tempcap, "(kz|kzr|kz:)");
                            if(match.Success)
                            {
                                string str = match.Value;
                                tempcap = tempcap.Substring(tempcap.IndexOf(str));
                                int e = tempcap.IndexOf('(');
                                capital = tempcap.Substring(0, e);
                            }
                        }
//----------------------------------------------Step-19 - ACTIVITIES-------------------------------------------------//

                        if (companyinfo.Contains("Objecto"))
                        {
                            int st = companyinfo.IndexOf("ARTIGO") + 5;
                            int offset = companyinfo.LastIndexOf("ARTIGO") + 20;
                            if (st == 4)
                            {
                                st = companyinfo.IndexOf("Objecto");
                                offset = companyinfo.IndexOf("Sócio");
                                if (!(offset == -1))
                                    activities = companyinfo.Substring(st, offset - st);
                                else
                                    offset = companyinfo.IndexOf("Sócia");
                                if (!(offset == -1))
                                    activities = companyinfo.Substring(st, offset - st);
                                else
                                    activities = companyinfo.Substring(st, companyinfo.LastIndexOf('.'));
                            }
                            else
                            {
                                string sub = companyinfo.Substring(st, offset - st);
                                string preStrin = "objecto";
                                string searchStrin = ".";

                                int preInde = sub.IndexOf(preStrin) + preStrin.Length;
                                int searchInde = preInde + sub.Substring(preInde).IndexOf(searchStrin) + 1;

                                activities = sub.Substring(preInde, searchInde - preInde);
                            }
                        }                
//----------------------------------------------------Step 21-23----------------------------------------------------//

                        if (companyinfo.Contains("quotas"))
                        {
                            int st = companyinfo.IndexOf("ARTIGO") + 5;
                            int offset = companyinfo.LastIndexOf("ARTIGO") + 20;
                            if (st == 4)
                            {
                                st = companyinfo.IndexOf("quotas");
                                owner = companyinfo.Substring(st);
                            }
                            else
                            {
                                string sub = companyinfo.Substring(st, offset - st);
                                int offset1 = sub.IndexOf("quotas");
                                for (int h = 1; h < 10; h++)
                                {
                                    offset1 = sub.IndexOf("quotas", offset1 + 1);
                                    string preString = "quota";
                                    string searchStr;
                                    Match match = Regex.Match(sub, "(Kz|KzR|Kz:)");
                                    if (match.Success)
                                    {
                                        searchStr = match.Value;


                                        string searchString = searchStr;                                      
                                        string preStrin = "Kz";
                                        string searchStrin = "(";
                                        int preInde = sub.IndexOf(preStrin) + 4;
                                        int searchInde = preInde + sub.Substring(preInde).IndexOf(searchStrin);

                                        ownerholds = sub.Substring(preInde, searchInde - preInde);
                                        preStrin = sub.Substring(sub.IndexOf(ownerholds));
                                        preInde = preStrin.IndexOf("sócio");
                                        string newstrin = preStrin.Substring(preInde);
                                        searchStrin = ".";
                                        int startin = newstrin.IndexOf("sócio") + 5;
                                        int endin = newstrin.IndexOf(searchStrin);
                                        if (endin == -1)
                                        {
                                            searchStrin = ",";
                                            endin = newstrin.IndexOf(searchStrin);
                                        }
                                        owner = newstrin.Substring(startin, endin - startin);
                                    }
                                }
                            }
                        }

  //------------------------------------------------Step-24- MANAGEMENT-----------------------------------------------//

                        if (companyinfo.ToLower().Contains("gerência"))
                        {
                            string tempman = companyinfo.ToLower().Substring(companyinfo.ToLower().IndexOf("gerência"));
                            if (tempman.Contains(shareholder1.ToLower()))
                                management = shareholder1;
                            else if (tempman.Contains(shareholder2.ToLower()))
                                management = shareholder2;
                            else if (tempman.Contains(shareholder3.ToLower()))
                                management = shareholder3;
                        }
                        else if (companyinfo.ToLower().Contains("gerente"))
                        {
                            string tempman = companyinfo.ToLower().Substring(companyinfo.ToLower().IndexOf("gerente"));
                            if (tempman.Contains(shareholder1.ToLower()))
                                management = shareholder1;
                            else if (tempman.Contains(shareholder2.ToLower()))
                                management = shareholder2;
                            else if (tempman.Contains(shareholder3.ToLower()))
                                management = shareholder3;
                        }
                        else if (companyinfo.ToLower().Contains("Administração"))
                        {
                            string tempman = companyinfo.Substring(companyinfo.IndexOf("Administração"));
                            if (tempman.Contains(shareholder1))
                                management = shareholder1;
                            else if (tempman.Contains(shareholder2))
                                management = shareholder2;
                            else if (tempman.Contains(shareholder3))
                                management = shareholder3;
                        }
 //-------------------------------------------------------------------------------------------------------------------///
                    }
                    company.DateOfIncorporation = doi;
                    company.NameOfCompany = indexline;
                    shareHolderDetails.Shareholder1 = shareholder1;
                    shareHolderDetails.Address1 = address1;
                    shareHolderDetails.Birthplace1 = birthplace1;
                    shareHolderDetails.Nationality1 = nationality1;
                    shareHolderDetails.ID1 = id1;
                    shareHolderDetails.Shareholder2 = shareholder2;
                    shareHolderDetails.Address2 = address2;
                    shareHolderDetails.Birthplace2 = birthplace2;
                    shareHolderDetails.Nationality2 = nationality2;
                    shareHolderDetails.ID2 = id2;
                    shareHolderDetails.Shareholder3 = shareholder3;
                    shareHolderDetails.Address3 = address3;
                    shareHolderDetails.Birthplace3 = birthplace3;
                    shareHolderDetails.Nationality3 = nationality3;
                    shareHolderDetails.ID3 = id3;
                    company.Owner = owner;
                    company.OwnerHolds = ownerholds;
                    company.Activities = activities;                                        
                    company.Headquarters = headquarters;
                    company.CompanyNumber = companynumber;
                    company.Capital = capital;
                    company.Management = management;
                    /*       Console.WriteLine(indexline);
                           Console.WriteLine(doi);
                           Console.WriteLine(shareholder1);
                           Console.WriteLine(address1);
                           Console.WriteLine(nationality);
                           Console.WriteLine(id1);
                           Console.WriteLine(birthplace1);
                           Console.WriteLine(shareholder2);
                           Console.WriteLine(address2);
                           Console.WriteLine(birthplace2);
                           Console.WriteLine(id2);
                           Console.WriteLine(shareholder3);
                           Console.WriteLine(address3);
                           Console.WriteLine(birthplace3);
                           Console.WriteLine(id3);
                           Console.WriteLine(activities);
                           Console.WriteLine(headquarters);
                           Console.WriteLine(owner);
                           Console.WriteLine(ownerholds);     */
                }
                context.companyDetails.Add(company);
                context.details.Add(shareHolderDetails);
                context.SaveChanges();

            }
            return null;
        }
    }




}

               


 

           
           
                                                                   
                          




      
            

       
            